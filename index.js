const dateFormatYmd = d3.timeFormat('%Y-%m-%d');
const dateFormatYmdParse = d3.timeParse('%Y-%m-%d');
const dateFormatdMYParse = d3.timeParse('%d-%b-%Y');
const dateFormatmdYParse = d3.timeParse('%m/%d/%Y');
const dateFormatYW = d3.timeFormat('%Y-%W');
const dateFormatYWParse = d3.timeParse('%Y-%W');
const numberFormat = d3.format(',.0f');
const percentFormat = d3.format('.1%');
const percent3Format = d3.format('.3%');
const permilleFormat = function(n) {
  return (Number(n)*1000).toFixed(2) + "‰";
};
const bisectKey = d3.bisector((d) => { return d.key; });

const styles = {
  daily_infected: {     // @see https://paletton.com/#uid=b2A030H0kkhJP26s087JitZNnVzJE
    mainColor: '#FF9710',
    avgColor: '#FFE3BE',
    avgDashStyle: [1, 2],
  },
  daily_cured: {
    mainColor: '#77F30F',
    avgColor: '#CEF0B3',
    avgDashStyle: [1, 2],
  },
  daily_dead: {
    mainColor: '#FF2210',
    avgColor: '#FFC3BE',
    avgDashStyle: [1, 2],
  },


  daily_penal: {          // @see https://paletton.com/#uid=50R0R0kllllaFw0g0qFqFg0w0aF
    mainColor: '#AA3939',
    avgColor: '#FFAAAA',
    avgDashStyle: [1, 2],
  },
  daily_isolation: {
    mainColor: '#AA8039',
    avgColor: '#FFE0AA',
    avgDashStyle: [1, 2],
  },
  daily_circulation: {
    mainColor: '#AAA239',
    avgColor: '#FFF9AA',
    avgDashStyle: [1, 2],
  },
  daily_tel112: {           // @see https://paletton.com/#uid=53Y0L0kllllaFw0g0qFqFg0w0aF
    mainColor: '#333676',
    avgColor: '#7F81B1',
    avgDashStyle: [1, 2],
  },
  daily_telverde: {
    mainColor: '#592A71',
    avgColor: '#9874AA',
    avgDashStyle: [1, 2],
  },

  daily_tests: {
    mainColor: '#6DF000',
    avgColor: '#CEF0B3',
    avgDashStyle: [1, 2],
  },

  people_in: {           // @see https://paletton.com/#uid=53Y0L0kcglL4Zvw8Eq6eXhmkwen
    mainColor: '#181D7A',// @see https://paletton.com/#uid=53Y0L0kt+lZlOstrKqzzSiaJidt
    avgColor: '#42469E',
    avgDashStyle: [1, 2],
  },
  people_out: {
    mainColor: '#520D75',
    avgColor: '#763597',
    avgDashStyle: [1, 2],
  },
  car_in: {
    mainColor: '#515379',
    avgColor: '#9798AE',
    avgDashStyle: [1, 2],
  },
  car_out: {
    mainColor: '#654A74',
    avgColor: '#9F8FA7',
    avgDashStyle: [1, 2],
  },
};

var dimensions = {
  panel1_width: 940,
  panel1_height: 400,
  panel2_width: 200,
  panel2_height: 150,
  panel3_width: 200,
  panel3_height: 200,
};

const translations = {
  date: 'Data',
  daily_infected: 'Infecţii',
  daily_dead: 'Decese',
  daily_cured: 'Vindecări',

  daily_penal: 'Dosare penale',
  daily_isolation: 'încălcarea izolării',
  daily_circulation: 'restricții de circulație',
  daily_tel112: 'Apeluri 112',
  daily_telverde: 'Apeluri Telverde',

  daily_tests: 'Teste efectuate',

  people_in: 'Intrări persoane',
  people_out: 'Ieșiri persoane',
  car_in: 'Intrări vehicule',
  car_out: 'Ieșiri vehicule',

  day: [
    'Luni',
    'Marţi',
    'Miercuri',
    'Joi',
    'Vineri',
    'Sâmbătă',
    'Duminică',
  ],
};


function loadCSV_National() {
  return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - National.csv")
    .then(function (allRows) {
      return remapCSVWithRules(allRows, [
        {
          key: 'date',
          primary: true,
          convert: function (v) {
            return dateFormatdMYParse(v);
          },
          regexp: new RegExp('^Data$')
        },
        {
          key: 'daily_infected',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Cazuri noi')
        },
        {
          key: 'daily_dead',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Decese noi')
        },
        {
          key: 'daily_cured',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Vindecări noi')
        },
      ]);
    });
}


function loadCSV_Sanctiuni() {
  return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - Sanctiuni.csv")
    .then(function (allRows) {
      return remapCSVWithRules(allRows, [
        {
          key: 'date',
          primary: true,
          convert: function (v) { return dateFormatdMYParse(v); },
          regexp: new RegExp('^Data$')
        },
        {
          key: 'daily_penal',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Dosare penale noi')
        },
        {
          key: 'daily_isolation',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Contravenții noi - încălcarea izolării')
        },
        {
          key: 'daily_circulation',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Contravenții noi - restricții de circulație')
        },
      ]);
    });
}


function loadCSV_Calls() {
  return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - Sanctiuni.csv")
    .then(function (allRows) {
      return remapCSVWithRules(allRows, [
        {
          key: 'date',
          primary: true,
          convert: function (v) { return dateFormatdMYParse(v); },
          regexp: new RegExp('^Data$')
        },
        {
          key: 'daily_tel112',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Apeluri 112')
        },
        {
          key: 'daily_telverde',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Apeluri Telverde')
        },
      ]);
    });
}


function loadCSV_Teste() {
  return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - Teste.csv")
    .then(function (allRows) {
      return remapCSVWithRules(allRows, [
        {
          key: 'date',
          primary: true,
          convert: function (v) { return dateFormatdMYParse(v); },
          regexp: new RegExp('^Data$')
        },
        {
          key: 'daily_tests',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Teste \\(pe zi\\)')
        },
      ]);
    });
}

function loadCSV_Judete() {
  return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - Judete.csv")
  // return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - Judete-dev.small.csv")
    .then(function (allRows) {
      return remapCSVWithRules(allRows, [
        {
          key: 'date',
          primary: true,
          convert: function (v) { return dateFormatmdYParse(v); },
          regexp: new RegExp('^Data$')
        },
        {
          key: 'city',
          primary: false,     propagate: true,
          convert: function (v) { return v; },
          regexp: new RegExp('^Județ')
        },
        {
          key: 'daily_infected',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Cazuri noi$')
        },
        {
          key: 'daily_dead',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Decese noi$')
        },
      ]);
    });
}


function loadCSV_Frontiere() {
  return d3.csv("data/Situația COVID-19 în România - CASA JURNALISTULUI - Frontiere.csv")
    .then(function (allRows) {
      return remapCSVWithRules(allRows, [
        {
          key: 'date',
          primary: true,
          convert: function (v) { return dateFormatdMYParse(v); },
          regexp: new RegExp('^Data$')
        },
        {
          key: 'people_in',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Intrări persoane$')
        },
        {
          key: 'people_out',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Ieșiri persoane$')
        },
        {
          key: 'car_in',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Intrări vehicule$')
        },
        {
          key: 'car_out',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^Ieșiri vehicule$')
        },
      ]);
    });
}

function remapCSVWithRules(allRows, remapRules) {
  var remappedKeys = {};
  var firstRow = d3.entries(allRows[0]);
  firstRow.forEach((d) => {
    for (var ri = 0; ri < remapRules.length; ri++) {
      if (d.key.match(remapRules[ri].regexp)) {
        remappedKeys[d.key] = {
          key: remapRules[ri].key,
          rule: remapRules[ri]
        };
        break;
      }
    }
  });
  var propagatedValues = [];
  var remappedRows = [];


  let primaryKeys = [];
  let values = [];
  for (k in remappedKeys) {
    if (remappedKeys[k].rule.primary) {
      primaryKeys.push({
        key: k,
        rule: remappedKeys[k].rule
      });
    }
    else if (remappedKeys[k].rule.propagate) {
      propagatedValues.push({
        key: k,
        rule: remappedKeys[k].rule
      });
    }
    else {
      values.push({
        key: k,
        rule: remappedKeys[k].rule
      });
    }
  }


  allRows.forEach((d) => {
    for (var iv in values) {
      let nd = {
        key: null,
        category: null,
        values: null,
        value: null
      };
      for (var ip in primaryKeys) {
        nd.key = primaryKeys[ip].rule.convert(d[primaryKeys[ip].key]);
      }

      if (propagatedValues.length) {
        nd.propagatedValues = {};
        for (var ip in propagatedValues) {
          nd.propagatedValues[propagatedValues[ip].rule.key] = propagatedValues[ip].rule.convert(d[propagatedValues[ip].key]);
        }
      }

      nd.category = values[iv].rule.key;

      nd.value = values[iv].rule.convert(d[values[iv].key]);

      remappedRows.push(nd);
    }
  });

  return remappedRows;
}


function generateChart_totalChart(totalChart, totalsDimension) {
  const totalsGroup = totalsDimension.group().reduceSum((d) => { return d.value; });
  totalChart
    .width(dimensions.panel2_width)
    .height(dimensions.panel2_height)
    .radius(Math.min(dimensions.panel2_width, dimensions.panel2_height) * 0.5)
    .innerRadius(Math.min(dimensions.panel2_width, dimensions.panel2_height) * 0.25)
    .legend(dc.legend()
      .x(0)
      .y(0)
      .itemHeight(10)
      .gap(1)
      .autoItemWidth(true)
      .horizontal(false)
      .legendText((d) => {
        return translations[d.name];
      })
    )
    .colors((d) => {
      return (styles[d]?styles[d].mainColor:'#ff0000');
    })
    .label((d) => {
      let sum = d3.sum(totalsGroup.all(), (d) => { return d.value });
      // return d.key + " " + numberFormat(d.value);
      // return numberFormat(d.value);
      return numberFormat(100 * d.value / sum) + "%";
    })
    .title((d) => {
      let sum = d3.sum(totalsGroup.all(), (d) => { return d.value });
      // return d.key + " " + numberFormat(d.value);
      // return numberFormat(d.value);
      return numberFormat(d.value)
        + " " + translations[d.key] + " "
        + "(" + percentFormat(d.value / sum) + ")";
    })
    .dimension(totalsDimension)
    .group(totalsGroup);

    return totalChart;
}

function generateChart_mapChart(mapChart, dimension_cities, citiesValues, citiesJson) {
  const extentForAllCities = d3.extent(citiesValues.all(), (d) => {
    return d.value;
  });
  extentForAllCities[1]+=1/100000000; // make sure min != max, always

  mapChart.width(dimensions.panel3_width)
    .height(dimensions.panel3_height)
    .transitionDuration(200)
    .dimension(dimension_cities)
    .group(citiesValues)
    .projection(d3.geoMercator()
      .fitSize([dimensions.panel3_width, dimensions.panel3_height], citiesJson)
    )
    .overlayGeoJson(citiesJson.features, "city", (d) => {
      let city = d.properties.mnemonic.toLowerCase();
      return "ro-" + (city == 'if' ? 'b' : city);
    })
    .colors(
      d3.scalePow().exponent(0.3)
        // d3.scaleLog().base(2)  // <-- this gives black colors for some cases
        .domain([
          Math.min(0, extentForAllCities[0]),
          extentForAllCities[1] * 0.10,
          extentForAllCities[1] * 0.25,
          extentForAllCities[1] * 1.00,
        ])
        .range(["#F9FFF3", "#CBFC99", '#FF8058', '#FF3D00']) // @see http://paletton.com/#uid=51n1b0kl1Wx1x+IcEXDsUWkWEVB
    )
    .valueAccessor((d) => {
      return d.value;
    })
    .on('pretransition', (chart) => {
      // re-scale color on the filtered data
      const extentForAllCities = d3.extent(chart.group().all(), (d) => {
        return d.value;
      });
      chart.colors()
        .domain([
          Math.min(0, extentForAllCities[0]),
          extentForAllCities[1] * 0.10,
          extentForAllCities[1] * 0.25,
          extentForAllCities[1] * 1.00,
        ]);
    });

    return mapChart;
}


function generateChart_dailyChartScale(dailyChartScale, dimension_Ymd) {
  const chartData_daily_chartScale = dimension_Ymd.group().reduceSum((d) => {
    return d.value;
  });

  dailyChartScale.width(dimensions.panel1_width) /* dc.barChart('#monthly-volume-chart', 'chartGroup'); */
    .height(40)
    .turnOnControls(true)
    .controlsUseVisibility(true)
    .dimension(dimension_Ymd)
    .group(chartData_daily_chartScale)
    .centerBar(false)
    .colors(styles.daily_infected.mainColor)
    .gap(1)
    .elasticY(true)
    .round(d3.timeDay.round)
    .alwaysUseRounding(true)
    .xUnits(d3.timeDays);
  dailyChartScale.yAxis().ticks(0);

  return dailyChartScale;
}


function generateChart_dataTable(dataTable, totalsDimension) {
  dataTable
    .dimension(totalsDimension)
    .size(Infinity)
    // .size(10)
    .sortBy((d) => {
      return d.key;
    })
    .order(d3.descending)
    .showSections(true)
    .section((d) => {
      let start = dateFormatYWParse(dateFormatYW(d.key));
      let end = moment(start).add(1, 'week');
      return dateFormatYmd(start) + " - " + dateFormatYmd(end);
    })
    .columns([
      {
        label: 'Date',
        format: (d) => { return dateFormatYmd(d.key); },
      },
      {
        label: 'Category',
        format: (d) => { return translations[d.category]; },
      },
      {
        label: 'Count',
        format: (d) => { return numberFormat(d.value); },
      },
    ]);

    return dataTable;
}



function generateChart_dataTableJquery(dataTable, totalsDimension) {
  dataTable
    .dimension(totalsDimension)
    .size(10)
    .sortBy((d) => {
      return d.key;
    })
    .order(d3.descending)
    .columns([
      {
        label: 'Date',
        format: (d) => { return dateFormatYmd(d.key); },
      },
      {
        label: 'Category',
        format: (d) => { return translations[d.category]; },
      },
      {
        label: 'Count',
        format: (d) => { return numberFormat(d.value); },
      },
    ]);

    return dataTable;
}

function generateChart_dayOfWeek(totalPeyWeekDay, dayOfWeekDimension) {
  const dayOfWeekGroup = dayOfWeekDimension.group().reduceSum((d) => { return d.value; });
  totalPeyWeekDay
    .width(dimensions.panel2_width)
    .height(dimensions.panel2_height)
    .ordering((d) => {
      return d.key;
    })
    .label((d) => {
      return translations.day[d.key];
    })
    .title((d) => {
      let sum = d3.sum(dayOfWeekGroup.all(), (d) => { return d.value });
      return numberFormat(d.value)
        + " " + translations.day[d.key] + " "
        + "(" + percentFormat(d.value / sum) + ")";
    })
    .ordinalColors([
      // @see https://paletton.com/#uid=b2A030H0kjfdpbPiWdzeenjbwsi8E
      '#5C3F19', '#5C3F19', '#5C3F19', '#5C3F19', '#5C3F19',
      '#315216', '#315216',
    ])
    .dimension(dayOfWeekDimension)
    .group(dayOfWeekGroup).elasticX(true)
    .xAxis().ticks(2);
}

function generateCommonDailyChart(dailyChart) {
  dailyChart.yAxis().tickFormat((y) => {
    if (y<1000) {
      return y;
    }
    if (y<1000000) {
      return Number(y/1000).toFixed(0) + "k";
    }
    if (y<10000000000) {
      return Number(y/1000000).toFixed(0) + "M";
    }
  });

  return dailyChart
    .width(dimensions.panel1_width)
    .height(dimensions.panel1_height - 40)
    .elasticY(true)
    .yAxisLabel("Count")
    .xAxisLabel("")
    .legend(dc.legend().x(80).y(20).itemHeight(13).gap(5))
    .renderHorizontalGridLines(true)
    .on('pretransition.hideshow', legendToggle)
    .on('pretransition', (chart) => {
      annotator
        .setupDayWidth(new Date('2020-05-01'), new Date('2020-05-02'))
        .annotate([
          // corona-related events
          // https://www.digi24.ro/stiri/actualitate/informatii-oficiale-despre-coronavirus-in-romania-1266261
          { keys: [new Date('2020-02-28'), new Date('2020-02-28')], type:"event:bad", label: 'cazul #1', },
          { keys: [new Date('2020-03-22'), new Date('2020-03-22')], type:"event:bad", label: 'decesul #1', },

          { keys: [new Date('2020-05-18'), new Date('2020-05-18')], type:"event:good", label: 'stare de alerta', },

          // https://isujis.ro/ordonante-militare/
          // { keys: [new Date('2020-03-18'), new Date('2020-03-18')], type:"", label: 'ordonanta #1', },
          // { keys: [new Date('2020-03-21'), new Date('2020-03-21')], type:"", label: 'ordonanta #2', },
          // { keys: [new Date('2020-03-24'), new Date('2020-03-24')], type:"", label: 'ordonanta #3', },
          // { keys: [new Date('2020-03-29'), new Date('2020-03-29')], type:"", label: 'ordonanta #4', },
          // { keys: [new Date('2020-04-30'), new Date('2020-04-30')], type:"", label: 'ordonanta #5', },
          // { keys: [new Date('2020-04-30'), new Date('2020-04-30')], type:"", label: 'ordonanta #6, Suceava', },
          // { keys: [new Date('2020-04-04'), new Date('2020-04-04')], type:"", label: 'ordonanta #7', },
          // { keys: [new Date('2020-04-09'), new Date('2020-04-09')], type:"", label: 'ordonanta #8', },
          // { keys: [new Date('2020-04-16'), new Date('2020-04-16')], type:"", label: 'ordonanta #9', },
          // { keys: [new Date('2020-04-27'), new Date('2020-04-27')], type:"", label: 'ordonanta #10', },

          // sarbatori
          { keys: [new Date('2020-04-19'), new Date('2020-04-21')], type:"holiday", label: 'Pastele', },
          { keys: [new Date('2020-05-01'), new Date('2020-05-01')], type:"holiday", label: '1 Mai', },


        ]);

        // update tooltips
        chart.selectAll('g.dc-tooltip').each((_, stackIdx, stacks) => {
          d3.select(stacks[stackIdx]).selectAll('circle title').each(function (d) {
            d3.select(this).html(
              dateFormatYmd(d.data.key)
              + ", "
              + d.layer
              + ", "
              + numberFormat(d.data.value)
            );
          });
        });
    })
    .brushOn(false);
}

function generateLineChartAndMeanChart(parent, dimension, groupData, styles, translation) {
  let charts = [];
  // add the main chart
  charts.push(
    dc.lineChart(parent)
      .colors(styles.mainColor)
      .dimension(dimension)
      .curve(d3.curveCardinal.tension(0.25))
      .brushOn(false)
    //   .title((d) => {
    //  })
      .renderDataPoints({ radius: 2, fillOpacity: 1.0, strokeOpacity: 0.0 })
      .group(groupData, translation)
  );

  // add the avg chart
  charts.push(
    dc.lineChart(parent)
      .colors(styles.avgColor)
      .dashStyle(styles.avgDashStyle)
      .renderArea(true)
      .curve(d3.curveCardinal.tension(0.5))
      .dimension(dimension)
      .xyTipsOn(false)
      .group(avgGraphData.generateRegressionMean_4dp_3df(groupData), translation + " (avg)")
  );

  return charts;
}

function selectDate(date1, date2)
{
  dailyChartScale.replaceFilter(dc.filters.RangedFilter(
    date1.startOf('day').toDate(),
    date2.startOf('day').toDate()
  )).redrawGroup();

  window.setTimeout(() => { // workaround. sometimes dc.js doesn;t set the colors of the map from the first try
    dailyChartScale.redrawGroup();
  }, 0.25*1000);
}



function animateFromDate(date1, length)
{
  let date2 = moment(date1).clone();
  let today = moment().add(1, "day");
  date2.add(length, "days");
  // dailyChartScale.focus([
  // dailyChartScale.focus([
  //   date1,
  //   date2,
  // ]);
  dailyChartScale.replaceFilter(dc.filters.RangedFilter(
    date1.startOf('day').toDate(),
    date2.startOf('day').toDate()
  )).redrawGroup();

  if (date2<today) {
    date1.add(1, "days");
    window.setTimeout(() => {
      animateFromDate(date1, length);
    }, 0.5*1000);
  }
}

function drawLegendToggles(chart) {
  chart.selectAll('g.dc-legend .dc-legend-item')
    .style('opacity', function (d, i) {
      var subchart = chart.select('g.sub._' + i);
      var visible = subchart.style('visibility') !== 'hidden';
      return visible ? 1 : 0.2;
    });
}

function legendToggle(chart) {
  chart.selectAll('g.dc-legend .dc-legend-item')
    .on('click.hideshow', function (d, i) {
      var subchart = chart.select('g.sub._' + i);
      var visible = subchart.style('visibility') !== 'hidden';
      subchart.style('visibility', function () {
        return visible ? 'hidden' : 'visible';
      });
      drawLegendToggles(chart);
    })
  drawLegendToggles(chart);
}

