# coronavirus-data-from-casajurnalistului

Vizualizare evolutie coronavirus, date primare de la <https://coronavirus.casajurnalistului.ro/>, gdocs@ <https://docs.google.com/spreadsheets/d/1TafrOhj0AivTvTgNUGett6gEzn7m4mO2Urr-xYaIY4k/edit>
Using d3/dc.js & bootstrap basic styling.

Poti sa accesezi demo la <https://quamis.gitlab.io/coronavirus-data-from-casajurnalistului/>

## NOTES

Use:
<https://dc-js.github.io/dc.leaflet.js/>