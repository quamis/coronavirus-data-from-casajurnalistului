avgGraphData = {
  generateWithCallback: function (group, avgCb) {
    return {
      all: function () {
        var ret = [];
        var _all = group.all();
        const interval_in_past = 4, interval_in_future = 3;
        for (var i = 0; i < _all.length; ++i) {
          ret.push(avgCb(_all[i], _all.slice(Math.max(0, i - interval_in_past), i + interval_in_future)));
        }
        ret.push(avgCb(_all.slice(-1)[0], _all.slice(-interval_in_past - interval_in_future)));
        return ret;
      }
    };
  },


  generateRegressionMean_4dp_3df: function (group) {
    return {
      all: function () {
        var ret = [];
        let _all = group.all();
        const interval_in_past = 4, interval_in_future = 3;
        let i = 0;
        if (_all.length) {
          for (i = 0; i < _all.length; i += (interval_in_past+interval_in_future)) {
            let values = [];
            for (let j = i - interval_in_past; j < i + interval_in_future; j++) {
              if (j > 0 && j < _all.length) {
                values.push(_all[j].value);
              }
            }

            let r = {
              key: _all[i].key,
              value: 0
            };
            r.value = d3.mean(values);
            lastKey = r.key;
            ret.push(r);
          }

          // inject the last point, if needed
          let lastElementInSrc = _all.slice(-1)[0];
          let lastElementInRet = ret.slice(-1)[0];
          if (lastElementInSrc.key != lastElementInRet.key) {
            let r = {
              key:lastElementInSrc.key,
              value: d3.mean([lastElementInRet.value, lastElementInSrc.value])
            };
            ret.push(r);
          }
        }

        return ret;
      }
    };
  },

  generateRegressionMedian_4dp_3df: function (group) {
    return {
      all: function () {
        var ret = [];
        var _all = group.all();
        const interval_in_past = 4, interval_in_future = 3;
        var i = 0;
        for (i = 0; i < _all.length; i += (interval_in_past + interval_in_future)) {
          let values = [];
          for (var j = i - interval_in_past; j < i + interval_in_future; j++) {
            if (j > 0 && j < _all.length) {
              values.push(_all[j].value);
            }
          }

          let r = {
            key: _all[i].key,
            value: 0
          };
          r.value = d3.mean(values);
          ret.push(r);
        }

        if (i <= _all.length) {
          // inject the last point
          ret.push(_all.slice(-1)[0]);
        }

        return ret;
      }
    };
  },
};